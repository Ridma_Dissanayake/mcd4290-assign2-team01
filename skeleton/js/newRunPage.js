// Code for the Measure Run page.
class Run
    {
        constructor(startLocation,destinationLocation,arrayOfLocation,startDate,endDate)
        {
            this.startLocation = startLocation;
            this.destinationLocation = destinationLocation;
            this.arrayOfLocation = arrayOfLocation;
            this..startDate = startDate;
            this.endDate = endDate;
        }   
        get  startLocation()
        {
            return this.startLocation;
        }
        set startLocation(newStartLocation)
        {
            return this.startLocation = newStartLocation;
        }
        get destinationLocation()
        {
            return this.destinationLocation;
        }
        set destinationLocation(newDestinationLocation)
        {
            return this.destinationLocation = newDestinationLocation;
        }
        get arrayOfLocation()
        {
            return this.arrayOfLocation;
        }
        set arrayOfLocation(newArrayOfLocation)
        {
            return this.arrayOfLocation = newArrayOfLocation;
        }
        get startDate()
        {
            return this.startDate;
        }
        set startDate(newStartDate)
        {
            return this.startDate = newStartDate;
        }
        get endDate()
        {
            return this.endDate;
        }
        set endDate(newEndDate)
        {
            return this.endDate = newEndDate;
        }
    }


